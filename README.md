This project is for the simulation of the micro-lens implementation on SiPM arrays. 

- Lensnew.cpp is the code for a spherical micro-lens implementation.
    - run with: 
        1. root -l -b -q Lensnew.cpp+(nevents, lens_radius, sphere_radius, residual_height, inefficient_region_efficiency)
        2. in root: .x Lensnew.cpp(...)
    - e.g. .x Lensnew.cpp(1000000,26.87,27.99,10,0.8)
- Lens_para.cpp is the code for a paraboloid micro-lens implementation.
    - run with: 
        1. root -l -b -q Lens_para.cpp+(nevents, lens_radius, lens_height, residual_height, inefficient_region_efficiency)
        2. in root: .x Lens_para.cpp(...)
Both codes implement 1 lens on a 9x9 pixel grid. 
The EGFF=N_{detected}/N_{total} can be scaled to 4.5 lenses (average of possible lenses on 9x9 grid).

- The azimuthal angle (phi) is generated using an input .txt file (measured or simulated values) from the exit angle distribution of the fibre in air. 


#include <iostream>
#include <TH2.h>
#include <TH2D.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <random>
#include <chrono>
#include "TMath.h"
#include <cmath>
#include <math.h> 
#include <TLine.h>
#include <TGraph2D.h>
#include <THStack.h>
#include <stdio.h>
#include <fstream>

using namespace std;
//Pixel size in x
#define pixel_sizex 40
//Pixel size in y
#define pixel_sizey 40


//global counters to count photons arriving at epoxy, number of photons reflected/transmitted at epoxy
int g_epoxy=0;
int g_epoxy_refl=0;
int g_epoxy_transm=0;

double angle,intensity;
vector <double> angle_list, intensity_list;

//Definition of the producte between a scalar and a vector
vector<double> Prod(double u, vector<double> v){

  vector<double> prod_vect;
  
  prod_vect.push_back(u*v[0]);
  prod_vect.push_back(u*v[1]);
  prod_vect.push_back(u*v[2]);
  return prod_vect;
}

//Definition of the vector product
vector<double> Prod_vect(vector<double>u, vector<double> v){

  vector<double> prod_vect;
  
  prod_vect.push_back(u[1]*v[2]-u[2]*v[1]);
  prod_vect.push_back(u[2]*v[0]-u[0]*v[2]);
  prod_vect.push_back(u[0]*v[1]-u[1]*v[0]);
  return prod_vect;
}

//Definition of the scalar product
double Prod_scal(vector<double>u, vector<double> v){

  double prod_scal= 0.0;
 
  prod_scal=u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
  return prod_scal;
}

//Schlick's approximation for the probability of reflection.
double Schlick_Reflection(double angle, double n1, double n2){

  double prob;
  double R0=((n1-n2)/(n1+n2));
  prob = pow(R0,2)+(1-pow(R0,2))*pow((1-cos(angle)),5);
  return prob;
}

double distributionOfTheAngle(double x){

//angular distribution for fibre coupled to lens
//Angular distribution of the angle between z and the x-y plane at the output of the fibre.
//Linear between 0° and45.5 °. Exponentialy decreasing between45.6° and 90°.
//Angular output of fibre in air -> experimental values

  if(x<0.0) {
    return 0.0;
  }
  
  else if(x<=angle_list.back()){
    double y_value=0;
    for (int i=0; i<angle_list.size()-1; i++){
      if(x>=angle_list[i] && x<=angle_list[i+1]){
        double d = angle_list[i+1]-angle_list[i];
        double d1 = x - angle_list[i];
        double d2 = angle_list[i+1] - x;
        y_value=intensity_list[i]*(d2/d) + intensity_list[i+1]*(d1/d);
      }
      else continue;
    }
    return y_value;
  }
  /*
  if(x<0.0) {
    return 0.0;
  }
  else if (x<=45.5){
    return 0.0344*x + 0.0655;
  }
  else if (x>45.5 && x<=90){
    return 177.72*TMath::Exp(-0.111*x);
  }*/
  else return 0.0;

//angular distribution for PDE setup (light output 25 cm in front of SiPM)
/*  if(x<0.0) {
    return 0.0;
  }
  else if (x>=0 && x <=0.5){
    return 1;
  }
  else if (x>0.5){
    return 0;
  }
  else return 0.0;*/
}

//Generate a random angle following the distribution given in "distributionOfTheAngle
double getRandomAngleOfEmission(double angle_cut){  

  TF1* func = new TF1("Func","distributionOfTheAngle(x)",0,angle_cut); 
  double alpha=func->GetRandom();
  delete(func);   
  //Transformation in radian
  return alpha*M_PI/180;
} 

//Function that generates the photon, random between pixel centre - generation area and pixel centre + generation area
vector<double> photon_generation(double generation_areax, double generation_areay,double height)
{
  vector<double> photon;
  double posx=0.0;
  double posy=0.0;
  double theta=0.0;
  double phi=0.0;

  //Randomly genetarated theta between 0 and 2PI. Theta gives the angle in the x-y plane
  std::uniform_real_distribution<double> theta_(0,2*M_PI);
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator_angle (seed);
  theta=theta_(generator_angle);
  //Generation of phi following the distribution given in "distributionOfTheAngle". 
  //The angle cut is given in the argument of the function
  phi=getRandomAngleOfEmission(90);

  //Generation of the position of the photon
  std::uniform_real_distribution<double> unifx(3*pixel_sizex/2-generation_areax,3*pixel_sizex/2+generation_areax);
  std::uniform_real_distribution<double> unify(3*pixel_sizey/2-generation_areay,3*pixel_sizey/2+generation_areay);
  unsigned seed2 = std::chrono::system_clock::now().time_since_epoch().count();
  std::random_device rd;
  std::mt19937 gen(rd());
  std::default_random_engine generator (seed2);
  posx= unifx(gen);
  unsigned seed3 = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator2 (seed3);
  posy= unify(generator2);

  //The return vector has the following structure : 1) position in x 2) position in y 3) height of the generation 4) theta [0°-360°] 5) phi [0°-90°]
  photon.push_back(posx);
  photon.push_back(posy);
  photon.push_back(height);
  photon.push_back(theta);
  photon.push_back(phi);
  return photon;
}

//Give the position of the photon at different heigths
vector<double> GetPhotonAtPosition(double posx, double posy, double angle_azy, double angle_pol, double height)
{     
  vector<double> position_inheight;
  posx=posx+cos(angle_azy)*tan(angle_pol)*height;
  posy=posy+sin(angle_azy)*tan(angle_pol)*height;
  position_inheight.push_back(posx);
  position_inheight.push_back(posy);
  return position_inheight;
}

//Function that gives the interstion of the photon with the lense. Distinguish between the case where the photon hits the lens or not.
vector<double> Photon_intersection(double posx, double posy, double theta, double phi, double height, double sphere_radius, double lens_height, int only_lens){
    vector<double>intersection;
    double x_int=0.0;
    double y_int=0.0;
    double z_int=0.0;
    double rho=0.0;
    //Position of the center of the sphere in the photon generation referential. The sphere is center in x ((3*pixel_sizex/2)um) and y ((3*pixel_sizey/2)um) for pixel
    //size of 50um in x and 50um in y.Distance z is tunable with the sphere radius and the lens height. !! Center of the sphere can be in z<0
    // but only the interaction point with z>0 are taken into account!!. With these two parameters the lens radius and the lens height are tunable.
    double x_ = 3*pixel_sizex/2-posx;
    double y_ = 3*pixel_sizey/2-posy;
    double z_ = height+(sphere_radius-lens_height);
    
    // //Computation of the solution of the intersection between a vector and a sphere with the referential at the photon generation point
    double a = 1.0;
    double b = -2*sin(phi)*cos(theta)*x_ -2*sin(phi)*sin(theta)*y_-2*cos(phi)*z_;
    double c = pow(x_,2)+pow(y_,2)+pow(z_,2)-pow(sphere_radius,2);
    rho = (-b-sqrt(pow(b,2)-4*a*c))/(2*a);

    //x,y,z are then re-expressed in the referential set at the begining of the first pixel
    x_int=rho*sin(phi)*cos(theta)+posx;
    y_int=rho*sin(phi)*sin(theta)+posy;
    //z_int=lens_height-rho*cos(phi);
    z_int=height-rho*cos(phi);
    
    //cout << "x_int " << x_int << " y_int " << y_int << " z_int " << z_int << endl;
    //Case where interaction point is below z<0 or that have no intersection with the sphere
    if(rho != rho || z_int<0){
      //Select all event
      if (only_lens==0){
	       intersection.push_back(posx+cos(theta)*tan(phi)*height);
	       intersection.push_back(posy+sin(theta)*tan(phi)*height);
      }
      //Select only event that have interacted with the sphere
      if (only_lens==1){
	       intersection.push_back(-1.0);
	       intersection.push_back(-1.0);
      }
      intersection.push_back(0.0);
    }
    //Event that have intersected
    else{  
        intersection.push_back(x_int);
        intersection.push_back(y_int);
        intersection.push_back(z_int);
    }
    intersection.push_back(theta);
    intersection.push_back(phi);
    return intersection;
  }
  
//Function that look at the photon propagation. Two cases : 1) Lens is deactivated (lens_status=0) 2) Lens is activated (lens_status=1) 
vector<double> Photon_propagation(double posx, double posy, double theta, double phi, double height, double sphere_radius,double lens_height, int lens_status, int only_lens)
{
  //DEACTIVATED: Case where the lens is deactivated
  vector<double> Final_position;
  if( lens_status == 0 ){
    posx=posx+cos(theta)*tan(phi)*height;
    posy=posy+sin(theta)*tan(phi)*height;
    Final_position.push_back(posx);
    Final_position.push_back(posy);
    //cout << " Theta in lens status 0" << theta << endl;
    //cout << " Phi in lens status 0" << phi << endl;
    Final_position.push_back(theta);//AM
    Final_position.push_back(phi);//AM
    return Final_position; //AM: returns x,y,theta,phi below lens
    }

  //ACTIVATED: Case where the lens is activated  
  else{

    //Get the intersection between the photon and the lens
    vector<double> Intersection;
    Intersection= Photon_intersection(posx,  posy,  theta,  phi,  height,  sphere_radius,lens_height,only_lens);
    //cout<<"intersection after: "<<Intersection[0]<<" "<<Intersection[1]<<" "<<Intersection[2]<<" "<<Intersection[3]<<endl;
    //If no interaction is seen (given if Intersection[2] ( height) is given by 0), nothing more needs to be done
    if(Intersection[2]==0){
      vector<double> interm;
      interm.push_back(Intersection[0]);
      interm.push_back(Intersection[1]);
      interm.push_back(Intersection[3]);
      interm.push_back(Intersection[4]);

      return interm;}
    
    //Propagation of the photon that has interact with the lense. The photon is either transmitted or reflected, depending of the angle with the normal. Propability given by
    //Sclick's approximation
    else{
      //Definition of the normal to a sphere at a point (n=gradf(p), with f is the equation of a sphere and p the point where the normal is search)
      vector<double> normal;
      normal.push_back(2*(Intersection[0]-3*pixel_sizex/2));
      normal.push_back(2*(Intersection[1]-3*pixel_sizey/2));
      normal.push_back(2*(Intersection[2]+(sphere_radius-lens_height))); 

      //Normalisation of the normal
      double norm_normal = sqrt(pow(normal[0],2)+pow(normal[1],2)+pow(normal[2],2));
      normal[0]=normal[0]/norm_normal;
      normal[1]=normal[1]/norm_normal;
      normal[2]=normal[2]/norm_normal;

      //Vector of the photon at the interaction point
      vector<double> photon_vector;
      photon_vector.push_back(Intersection[0]-posx);
      photon_vector.push_back(Intersection[1]-posy);
      photon_vector.push_back(Intersection[2]-height);
      
      //Normalisation of the photon vector
      double norm_photon_vector = sqrt(pow(photon_vector[0],2)+pow(photon_vector[1],2)+pow(photon_vector[2],2));
      photon_vector[0]=photon_vector[0]/norm_photon_vector;
      photon_vector[1]=photon_vector[1]/norm_photon_vector;
      photon_vector[2]=photon_vector[2]/norm_photon_vector;
    
      //Computation of the angle between the incident photon and the normal on the sphere at that point. This angle is then used for the Schlick's reflection probability
      double alpha_i =0.0;
      double prod_scal=normal[0]*photon_vector[0]+normal[1]*photon_vector[1]+normal[2]*photon_vector[2];
      alpha_i = acos(prod_scal/(norm_normal*norm_photon_vector));
     
      double proba=0.0;
      //Compute Schlick's reflection probability for the angle
      //Refractive index of air and uL
      double n1 = 1.0;
      double n2 = 1.5;
      proba=Schlick_Reflection(alpha_i, n1, n2);
      proba=proba/100.0;
      
      //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<> dis(0, 1);
      double gene_prob = dis(gen);
      
      //REFLECTION: Case where the photon is reflected
      if (gene_prob<proba){
        vector<double> reflected;
        vector<double> part;
          //cout<<"reflection in action"<<endl;
        //Direction vector of the reflected photon
        part=Prod(2*Prod_scal(photon_vector,normal),normal);
        
        reflected.push_back(photon_vector[0]-part[0]);
        reflected.push_back(photon_vector[1]-part[1]);
        reflected.push_back(photon_vector[2]-part[2]);

        //If the direction vector of the reflected photon is negativ, it means that it can still be detected. Point of interaction is given by the intersection of a vector (line) and a plan
        //normal (0,0,1).
        if (reflected[2]<0){
           Final_position.push_back(-1000);
           Final_position.push_back(-1000);
           Final_position.push_back(-1000);
           Final_position.push_back(-1000);
	       return Final_position;
        }
        //Lost photon
        else {
	       Final_position.push_back(-1000);
	       Final_position.push_back(-1000);
	       Final_position.push_back(-1000);
           Final_position.push_back(-1000);
	       return Final_position;
        }
     }
      //TRANSMISSION: Case where the photon is transmitted. The transmitted vector is given by Snell's law in 3D (vectorial form)
      else{
        vector<double> transmitted;

        vector<double> first_part;
        first_part = Prod((1.0/1.5),Prod_vect(normal,Prod_vect(Prod(-1,normal),photon_vector)));
        double p = 0.0;
        p = Prod_scal(Prod_vect(normal,photon_vector),Prod_vect(normal,photon_vector));
        vector<double> second_part;
        second_part = Prod(sqrt(1-(pow((1/1.5),2)*p)),normal);

        transmitted.push_back(first_part[0]-second_part[0]);
        transmitted.push_back(first_part[1]-second_part[1]);
        transmitted.push_back(first_part[2]-second_part[2]);

        
        // Point of interaction is given by the intersection of a vector (line) and a plan
        double positionx=-(Intersection[2]/transmitted[2])*transmitted[0]+Intersection[0];
        double positiony=-(Intersection[2]/transmitted[2])*transmitted[1]+Intersection[1];
        double positionz=-(Intersection[2]/transmitted[2])*transmitted[2]+Intersection[2];
      
        Final_position.push_back(positionx);
        Final_position.push_back(positiony);
        vector<double> vector_z;
        vector_z.push_back(0.0);
        vector_z.push_back(0.0);
        vector_z.push_back(1.0);
        
        double angle_phi = acos(transmitted[2]/sqrt(Prod_scal(transmitted,transmitted)));
        //cout << "Theta at the source" << (M_PI-angle_theta) << endl;
        //FAUT VERIFIER PHI
        double angle_theta = atan2(transmitted[1],transmitted[0]);
        //double angle_theta = atan(transmitted[1]/transmitted[0]);
        //cout << "Phi at the source " << (M_PI - angle_phi) << endl;
        //cout << "Theta at the source " << angle_theta << endl;

        Final_position.push_back(angle_theta);//(M_PI-angle_theta);
        Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
        return Final_position;
      }
    }
  }
}

//AM: Adding a rectangular piece to the lense
//input (describing the position and direction of a photon on the initial plane): vector<double>: x,y,theta [0,pi/2), phi [0,2pi), h
//output (intersection between photon and plane translated in z direction by -h): vector<double>: xn, yn
/*vector<double> rect(double x, double y, double theta, double phi, double h){

  vector<double> newint;
  newint.push_back(x+h/tan(theta)*sin(phi));
  newint.push_back(y+h/tan(theta)*cos(phi));

  return newint;
}*/

vector<double> rectunder(double x, double y, double xstart, double ystart, double h, double heightabs){
    
    vector<double> Final_position;
    
    vector<double> normal;
    normal.push_back(0);
    normal.push_back(0);
    normal.push_back(1);
    
    //Normalisation of the normal
    double norm_normal = sqrt(pow(normal[0],2)+pow(normal[1],2)+pow(normal[2],2));
    normal[0]=normal[0]/norm_normal;
    normal[1]=normal[1]/norm_normal;
    normal[2]=normal[2]/norm_normal;
    
    //Vector of the photon at the interaction point
    vector<double> photon_vector;
    photon_vector.push_back(x-xstart);
    photon_vector.push_back(y-ystart);
    photon_vector.push_back(-heightabs);
    //cout<<"rect initial x "<<xstart<<endl;
    //cout<<"rect initial y "<<ystart<<endl;
    //cout<<"rect initial z "<<heightabs<<endl;
    //Normalisation of the photon vector
    double norm_photon_vector = sqrt(pow(photon_vector[0],2)+pow(photon_vector[1],2)+pow(photon_vector[2],2));
    photon_vector[0]=photon_vector[0]/norm_photon_vector;
    photon_vector[1]=photon_vector[1]/norm_photon_vector;
    photon_vector[2]=photon_vector[2]/norm_photon_vector;
    
    
    //Computation of the angle between the incident photon and the normal on the sphere at that point. This angle is then used for the Schlick's reflection probability
    double alpha_i =0.0;
    double prod_scal=normal[0]*photon_vector[0]+normal[1]*photon_vector[1]+normal[2]*photon_vector[2];
    alpha_i = acos(prod_scal/(norm_normal*norm_photon_vector));
    
    double proba=0.0;
    //Compute Schlick's reflection probability for the angle
    proba=Schlick_Reflection(alpha_i, 1.5, 1.5);
    proba=proba/100.0;
    
    //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    double gene_prob = dis(gen);
    

    ::g_epoxy+=1;

    //REFLECTION: Case where the photon is reflected
    if (gene_prob<proba){
        
        ::g_epoxy_refl+=1;

        vector<double> reflected;
        vector<double> part;
        
        //Direction vector of the reflected photon
        part=Prod(2*Prod_scal(photon_vector,normal),normal);
        
        reflected.push_back(photon_vector[0]-part[0]);
        reflected.push_back(photon_vector[1]-part[1]);
        reflected.push_back(photon_vector[2]-part[2]);
        
        //If the direction vector of the reflected photon is negative, it means that it can still be detected. Point of interaction is given by the intersection of a vector (line) and a plan
        //normal (0,0,1).
        if (reflected[2]<0){
            //cout<<"rect reflected"<<endl;
            double positionx=-1000;
            double positiony=-1000;
            double positionz=-1000;
            Final_position.push_back(positionx);
            Final_position.push_back(positiony);
            
            double angle_phi = acos(reflected[2]/sqrt(Prod_scal(reflected,reflected)));
            double angle_theta = atan2(reflected[1],reflected[0]);
            //double angle_theta = atan(reflected[1]/reflected[0]);
            Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
            Final_position.push_back(angle_theta);//(M_PI-angle_theta);
            
            return Final_position;
        }
        //Lost photon
        else {
            //cout<<"rect lost"<<endl;
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            return Final_position;
        }
    }
    //TRANSMISSION: Case where the photon is transmitted. The transmitted vector is given by Snell's law in 3D (vectorial form)
    else{
        
        ::g_epoxy_transm+=1;
        
        vector<double> transmitted;
        //cout<<"rect transmitted"<<endl;
        vector<double> first_part;
        first_part = Prod((1.5/1.5),Prod_vect(normal,Prod_vect(Prod(-1,normal),photon_vector)));
        double p = 0.0;
        p = Prod_scal(Prod_vect(normal,photon_vector),Prod_vect(normal,photon_vector));
        vector<double> second_part;
        second_part = Prod(sqrt(1-(pow((1.5/1.5),2)*p)),normal);
        
        transmitted.push_back(first_part[0]-second_part[0]);
        transmitted.push_back(first_part[1]-second_part[1]);
        transmitted.push_back(first_part[2]-second_part[2]);
        
        
        // Point of interaction is given by the intersection of a vector (line) and a plan
        double positionx=-(h/transmitted[2])*transmitted[0]+x;
        double positiony=-(h/transmitted[2])*transmitted[1]+y;
        double positionz=-(h/transmitted[2])*transmitted[2];
        
        //cout<<"rect posx "<<positionx<<endl;
        //cout<<"rect posy "<<positiony<<endl;
        //cout<<"rect posz "<<positionz<<endl;
        
        Final_position.push_back(positionx);
        Final_position.push_back(positiony);
        vector<double> vector_z;
        vector_z.push_back(0.0);
        vector_z.push_back(0.0);
        vector_z.push_back(1.0);
        
        double angle_phi = acos(transmitted[2]/sqrt(Prod_scal(transmitted,transmitted)));
        //cout << "Theta at the source" << (M_PI-angle_theta) << endl;
        //FAUT VERIFIER PHI
        double angle_theta = atan2(transmitted[1],transmitted[0]);
        //double angle_theta = atan(transmitted[1]/transmitted[0]);
        //cout << "Phi at the source" << angle_phi*180/M_PI << endl;
        
        Final_position.push_back(angle_theta);//(M_PI-angle_theta);
        Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
        return Final_position;
    }
    
}

//AM: Adding a rectangular piece to the lense
//input (describing the position and direction of a photon on the initial plane): vector<double>: x,y,theta [0,pi/2), phi [0,2pi), h
//output (intersection between photon and plane translated in z direction by -h): vector<double>: xn, yn
vector<double> rect(double x, double y, double xstart, double ystart, double h, double heightabs){
    
    vector<double> Final_position;
    
    vector<double> normal;
    normal.push_back(0);
    normal.push_back(0);
    normal.push_back(1);
    
    //Normalisation of the normal
    double norm_normal = sqrt(pow(normal[0],2)+pow(normal[1],2)+pow(normal[2],2));
    normal[0]=normal[0]/norm_normal;
    normal[1]=normal[1]/norm_normal;
    normal[2]=normal[2]/norm_normal;
    
    //Vector of the photon at the interaction point
    vector<double> photon_vector;
    photon_vector.push_back(x-xstart);
    photon_vector.push_back(y-ystart);
    photon_vector.push_back(-heightabs);
    //cout<<"rect initial x "<<xstart<<endl;
    //cout<<"rect initial y "<<ystart<<endl;
    //cout<<"rect initial z "<<heightabs<<endl;
    //Normalisation of the photon vector
    double norm_photon_vector = sqrt(pow(photon_vector[0],2)+pow(photon_vector[1],2)+pow(photon_vector[2],2));
    photon_vector[0]=photon_vector[0]/norm_photon_vector;
    photon_vector[1]=photon_vector[1]/norm_photon_vector;
    photon_vector[2]=photon_vector[2]/norm_photon_vector;
    
    
    //Computation of the angle between the incident photon and the normal on the sphere at that point. This angle is then used for the Schlick's reflection probability
    double alpha_i =0.0;
    double prod_scal=normal[0]*photon_vector[0]+normal[1]*photon_vector[1]+normal[2]*photon_vector[2];
    alpha_i = acos(prod_scal/(norm_normal*norm_photon_vector));
    
    double proba=0.0;
    //Compute Schlick's reflection probability for the angle
    proba=Schlick_Reflection(alpha_i,1.,1.5);
    proba=proba/100.0;
    
    //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    double gene_prob = dis(gen);
    
    ::g_epoxy+=1;

    //REFLECTION: Case where the photon is reflected
    if (gene_prob<proba){
        
        ::g_epoxy_refl+=1;
        
        vector<double> reflected;
        vector<double> part;
        
        //Direction vector of the reflected photon
        part=Prod(2*Prod_scal(photon_vector,normal),normal);
        
        reflected.push_back(photon_vector[0]-part[0]);
        reflected.push_back(photon_vector[1]-part[1]);
        reflected.push_back(photon_vector[2]-part[2]);
        
        //If the direction vector of the reflected photon is negative, it means that it can still be detected. Point of interaction is given by the intersection of a vector (line) and a plan
        //normal (0,0,1).
        if (reflected[2]<0){
            //cout<<"rect reflected"<<endl;
            double positionx=-1000;
            double positiony=-1000;
            double positionz=-1000;
            Final_position.push_back(positionx);
            Final_position.push_back(positiony);
            
            double angle_phi = acos(reflected[2]/sqrt(Prod_scal(reflected,reflected)));
            double angle_theta = atan2(reflected[1],reflected[0]);
            //double angle_theta = atan(reflected[1]/reflected[0]);
            Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
            Final_position.push_back(angle_theta);//(M_PI-angle_theta);
            
            return Final_position;
        }
        //Lost photon
        else {
            //cout<<"rect lost"<<endl;
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            return Final_position;
        }
    }
    //TRANSMISSION: Case where the photon is transmitted. The transmitted vector is given by Snell's law in 3D (vectorial form)
    else{

        ::g_epoxy_transm+=1;

        vector<double> transmitted;
        //cout<<"rect transmitted"<<endl;
        vector<double> first_part;
        first_part = Prod((1.0/1.5),Prod_vect(normal,Prod_vect(Prod(-1,normal),photon_vector)));
        double p = 0.0;
        p = Prod_scal(Prod_vect(normal,photon_vector),Prod_vect(normal,photon_vector));
        vector<double> second_part;
        second_part = Prod(sqrt(1-(pow((1/1.5),2)*p)),normal);
        
        transmitted.push_back(first_part[0]-second_part[0]);
        transmitted.push_back(first_part[1]-second_part[1]);
        transmitted.push_back(first_part[2]-second_part[2]);
        
        
        // Point of interaction is given by the intersection of a vector (line) and a plan
        double positionx=-(h/transmitted[2])*transmitted[0]+x;
        double positiony=-(h/transmitted[2])*transmitted[1]+y;
        double positionz=-(h/transmitted[2])*transmitted[2];
        
        //cout<<"rect posx "<<positionx<<endl;
        //cout<<"rect posy "<<positiony<<endl;
        //cout<<"rect posz "<<positionz<<endl;
        
        Final_position.push_back(positionx);
        Final_position.push_back(positiony);
        vector<double> vector_z;
        vector_z.push_back(0.0);
        vector_z.push_back(0.0);
        vector_z.push_back(1.0);
        
        double angle_phi = acos(transmitted[2]/sqrt(Prod_scal(transmitted,transmitted)));
        //cout << "Theta at the source" << (M_PI-angle_theta) << endl;
        //FAUT VERIFIER PHI
        double angle_theta = atan2(transmitted[1],transmitted[0]);
        //double angle_theta = atan(transmitted[1]/transmitted[0]);
        //cout << "Phi at the source" << angle_phi*180/M_PI << endl;
        
        Final_position.push_back(angle_theta);//(M_PI-angle_theta);
        Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
        return Final_position;
    }
    
}
 
//Main of the program
int Lensnew(int event, double lens_radius, double sphere_radius, double Hrect, double GFF, double ineff_efficiency, double lens_status)
{

  ifstream infile;
  infile.open("Fibre_Angle_Distribution_exp.txt");

  while (infile>>angle>>intensity) {
    angle_list.push_back(angle);
    intensity_list.push_back(intensity);
  }

  infile.close();

  //Geometrical fill factor
  //double GFF = 0.50;
  cout << "The detector has a GFF=" << GFF*100 << "% and an efficiency in the inefficient region of " << ineff_efficiency*100 << "%.";

  //Lens height is linked to the sphere radius and the lens radius
  double lens_height = sphere_radius-sqrt(pow(sphere_radius,2)-pow(lens_radius,2));
  cout << "The height of the lens is " << lens_height << endl;
  
  //The height (z position of the generated photon) is given by the lens_height + 1um
  double distance = lens_height+1;
  double height = distance;
  //cout<<"height: "<<height<<endl;
  
  //Dead area computation
  double dead_areax = pixel_sizex*((1-sqrt(GFF))/2);
  double dead_areay = pixel_sizey*((1-sqrt(GFF))/2);
  cout << "The dead area is " << dead_areax << endl;

  //Inefficienct area
  //2 um around active area
  double ineff_areax = 2.; 
  double ineff_areay = 2.;
  //double ineff_efficiency = 1.;

  //Status of the lens and the non-interactive photon
  //double lens_status = 0.; //1=on,0=off
  double only_lens = 0;// 1 = only lens, 0 = all event

  vector<double> distribution;
  vector<double> propagation;

  //Count of photon hitting dead area or active area
  int deadcount=0;
  int goodcount=0;
  int ineffcount=0;
  int goodcount_ineff=0;
  int deadcount_ineff=0;

  vector<int> list_of_out;
  vector<double> intersection;

  //Canvas lists
  TCanvas *c1 = new TCanvas("c1","c1",700,700);
  TCanvas *c2 = new TCanvas("c2","c2",700,700);
  TCanvas *c3 = new TCanvas("c3","c3",700,700);
  TCanvas *c4 = new TCanvas("c4","c4",700,700);
  TCanvas *c5 = new TCanvas("c5","c5",700,700);
  TCanvas *c6 = new TCanvas("c6","c6",700,700);
  TCanvas *c7 = new TCanvas("c7","c7",700,700);
  TCanvas *c8 = new TCanvas("c8","c8",700,700);
  TCanvas *c9 = new TCanvas("c9","c9",700,700);
  TCanvas *c10 = new TCanvas("c10","c10",700,700);
  TCanvas *c11 = new TCanvas("c11","c11",700,700);
  TCanvas *c12 = new TCanvas("c12","c12",700,700);
  
  //Plot of the generation point of the photons
  TH2D *Photon_generation=new TH2D("Photon Generaton","generation of photons",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *photongeneration_proj=new TH1D("Photon Generaton  projection", "Projection Photon Generaton ", 3*pixel_sizex,0,3*pixel_sizex);
  //Plot of the interaction point on the lens
  TGraph2D *g = new TGraph2D();
  //Hit position on the detector after the interaction with the lens
  TH2D *Lens=new TH2D("Photon distribution (before propagation)","Photon distribution (before propagation)",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
   //Hit position on the detector after the interaction with the lens
  TH2D *Lens2=new TH2D("Photon distribution (after propagation)","Photon distribution (after propagation)",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *Lens2Proj=new TH1D("Photon distribution (after propagation) projection", "Projection Photon distribution (after propagation)", 3*pixel_sizex,0,3*pixel_sizex);
  //Hit position on the dead area
  TH2D *bad=new TH2D("Dead area hits","Dead area hits",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  //Hit position in the active area
  TH2D *good=new TH2D("Active area hits","active area hits",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *goodProj=new TH1D("Active area hits projection", "Projection active area hits", 3*pixel_sizex,0,3*pixel_sizex);
  //Angular distribution
  TH1D *phi=new TH1D("phidistri","Angular distribution",90,0,90);
  //Angular distribution after propagatoin
  TH1D *phiprop=new TH1D("phipdistri","Angular distribution",90,0,90);
  //TH2D *bad2=new TH2D("bad2","Dead Area2 Hit",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *badProj=new TH1D("bad projection", "Projection Dead area hits", 3*pixel_sizex,0,3*pixel_sizex);
  
  //Hh= height of pixel-hole
  //Lens might not sit on Silicon directly; could sit on resistor or wires
  double Hh=0.;//=7.;
  int cphi=0;
  int cphiprop=0;

  for (int i =0; i< event; ++i){
    if(i%100000==0)cout << " Event Nbr " << i << endl;
    //Generate the photon distribution (middle of array +- generation area)
    distribution = photon_generation(200,200,distance);
    //Fill the histogram of the photon generation
    //cout << "x initial " << distribution[0] << endl;
    //cout << "y initial " << distribution[1] << endl;
    Photon_generation->Fill(distribution[0], distribution[1]);
    //Intersection computation
    intersection = Photon_intersection(distribution[0],distribution[1], distribution[3], distribution[4], distribution[2], sphere_radius,lens_height, only_lens);
    //cout<<"intersection before: "<<intersection[0]<<" "<<intersection[1]<<" "<<intersection[2]<<" "<<intersection[3]<<endl;
    //Propagation of the photons
    propagation =  Photon_propagation(distribution[0],distribution[1], distribution[3], distribution[4], distribution[2], sphere_radius,lens_height, lens_status, only_lens);
    
    //cout << "theta at the propagation " << propagation[2]*180/M_PI << endl;
    //cout << "phi at the propagation " << propagation[3]*180/M_PI << endl;
    //cout << "psix " << propagation[0] << endl;
    //cout << "psiy " << propagation[1] << endl;
    
    //cout << "position 1 x" << propagation[0] << endl;
    //cout << "position 1 y" << propagation[1] << endl;
    //Fill the TGraph2D of the intersection points
    g->SetPoint(i,intersection[0],intersection[1],intersection[2]);
    //Fill the hit map
    Lens->Fill(propagation[0],propagation[1]);
    //Fill the angular distribution
    double angle = distribution[4]*180/M_PI;
    phi->Fill(angle);
    cphi+=1;

    //Definition of the` pixel geometry : 3 by 3 pixel
    //Pixel out of range 3x3 pixel
    if(Hrect ==0 && (propagation[0]<0.0*pixel_sizex || propagation[0]>3.0*pixel_sizex || propagation[1] <0.0*pixel_sizey || propagation[1]>3*pixel_sizey)){
      list_of_out.push_back(i);
    }
    //Dead area in x
    else if(Hrect==0 && ((propagation[0]<dead_areax) || ((pixel_sizex-dead_areax)<propagation[0] && propagation[0]<(pixel_sizex+dead_areax)) || ((2.0*pixel_sizex-dead_areax)< propagation[0] && propagation[0] < (2.0*pixel_sizex+dead_areax)) || (propagation[0]>3*pixel_sizex-dead_areax))){
      bad->Fill(propagation[0],propagation[1]);
      deadcount+=1;
    }
    //Dead area in y
    else if(Hrect==0 && ((propagation[1]<dead_areay) || ((pixel_sizey-dead_areay)<propagation[1] && propagation[1]<(pixel_sizey+dead_areay)) || ((2.0*pixel_sizey-dead_areay)< propagation[1] && propagation[1] < (2.0*pixel_sizey+dead_areay)) || (propagation[1]>3*pixel_sizey-dead_areay))){
        bad->Fill(propagation[0],propagation[1]);
        deadcount+=1;
    }

    else if(propagation[0]==-1000 && propagation[1]==-1000){
        list_of_out.push_back(i);
        //deadcount+=1;
    }
    //Good hit in active area
    else {
        
        vector<double> pr2;
        pr2.clear();
        
        if(Hrect==0 || (propagation[0]==-1000 && propagation[1]==-1000))pr2=propagation;
        else{
            if(intersection[2]!=0 && lens_status==1)pr2 = rectunder(propagation[0], propagation[1], intersection[0], intersection[1],Hrect,intersection[2]);
            else pr2 = rect(propagation[0], propagation[1], distribution[0],distribution[1] ,Hrect, distance);
        }
        Lens2->Fill(pr2[0],pr2[1]);    
        Lens2Proj=Lens2->ProjectionX();
        
        if(pr2[3] >= 0.00000001){
            //cout << "HEHEHEHEHE" << propagation[3] << endl;
            double anglep = pr2[3]*180/M_PI;
            phiprop->Fill(anglep);
            cphiprop+=1;
        }
        //cout << "theta after second propagation " << pr2[2]*180/M_PI << endl;
        //cout << "phi after second propagation " << pr2[3]*180/M_PI << endl;
        //cout << "psix " << pr2[0] << endl;
        //cout << "psiy " << pr2[1] << endl;

        //cout << "position 2 x" << pr2[0] << endl;
        //cout << "position 2 y" << pr2[1] << endl;
        
        if(pr2[0]<0.0*pixel_sizex || pr2[0]>3.0*pixel_sizex || pr2[1] <0.0*pixel_sizey || pr2[1]>3*pixel_sizey){
            list_of_out.push_back(i);
        }
        //Dead area in x
        else if ((pr2[0]<dead_areax) || ((pixel_sizex-dead_areax)<pr2[0] && pr2[0]<(pixel_sizex+dead_areax)) || ((2.0*pixel_sizex-dead_areax)< pr2[0] && pr2[0] < (2.0*pixel_sizex+dead_areax)) || (pr2[0]>3*pixel_sizex-dead_areax))
        {
          deadcount+=1;
          bad->Fill(pr2[0],pr2[1]);
          //bad2->Fill(pr2[0],pr2[1]);
        }
        //Dead area in y
        else if ((pr2[1]<dead_areay) || ((pixel_sizey-dead_areay)<pr2[1] && pr2[1]<(pixel_sizey+dead_areay)) || ((2.0*pixel_sizey-dead_areay)< pr2[1] && pr2[1] < (2.0*pixel_sizey+dead_areay)) || (pr2[1]>3*pixel_sizey-dead_areay))
        {
          deadcount+=1;
          bad->Fill(pr2[0],pr2[1]);
          //bad2->Fill(pr2[0],pr2[1]);
        }
        //Inefficient area in x
        else if ( (pr2[0]<(dead_areax+ineff_areax) && pr2[0]>dead_areax) || (pr2[0]>(pixel_sizex-dead_areax-ineff_areax) && pr2[0]<(pixel_sizex-dead_areax)) || (pr2[0]<(pixel_sizex+dead_areax+ineff_areax) && pr2[0]>(pixel_sizex+dead_areax)) || (pr2[0]>(2.0*pixel_sizex-dead_areax-ineff_areax) && pr2[0]<(2.0*pixel_sizex-dead_areax)) || (pr2[0]<(2.0*pixel_sizex+dead_areax+ineff_areax) && pr2[0]>(2.0*pixel_sizex+dead_areax)) || (pr2[0]>(3*pixel_sizex-dead_areax-ineff_areax) && pr2[0]<(3*pixel_sizex-dead_areax))){
            ineffcount+=1;
            //cout << "ineff count " << ineffcount << endl;
            
            //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_real_distribution<> dis(0, 1);
            double gene_prob = dis(gen);
            
            if (gene_prob<ineff_efficiency) { //want to have more photons in good region, example: 0.4<0.7 so we want to count it as good
                goodcount+=1;
                goodcount_ineff+=1;
                good->Fill(pr2[0],pr2[1]);
            }
            else {
                deadcount+=1;
                deadcount_ineff+=1;
                bad -> Fill(pr2[0],pr2[1]);
            }
        }
        //Inefficient area in y
        else if ((pr2[1]<(dead_areay+ineff_areay) && pr2[1]>dead_areay) || (pr2[1]>(pixel_sizey-dead_areay-ineff_areay) && pr2[1]<(pixel_sizey-dead_areay)) || (pr2[1]<(pixel_sizey+dead_areay+ineff_areay) && pr2[1]>(pixel_sizey+dead_areay)) || (pr2[1]>(2.0*pixel_sizey-dead_areay-ineff_areay) && pr2[1]<(2.0*pixel_sizey-dead_areay)) || (pr2[1]<(2.0*pixel_sizey+dead_areay+ineff_areay) && pr2[1]>(2.0*pixel_sizey+dead_areay)) || (pr2[1]>(3*pixel_sizey-dead_areay-ineff_areay) && pr2[1]<(3*pixel_sizey-dead_areay))){
            ineffcount+=1;
            //cout << "ineff count " << ineffcount << endl;
            
            //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_real_distribution<> dis(0, 1);
            double gene_prob = dis(gen);
            
            if (gene_prob<ineff_efficiency) {
                goodcount+=1;
                goodcount_ineff+=1;
                good->Fill(pr2[0],pr2[1]);
            }
            else {
                deadcount+=1;
                deadcount_ineff+=1;
                bad -> Fill(pr2[0],pr2[1]);
            }
        }
        else if(pr2[0]==-1000 && pr2[1]==-1000){
            deadcount+=1;
        }
        else{
            goodcount+=1;
            good->Fill(pr2[0],pr2[1]);
            goodProj=good->ProjectionX();

        }
    }
  }
  photongeneration_proj=Photon_generation->ProjectionX();
  badProj=bad->ProjectionX();
  //else badProj=bad2->ProjectionX();

  //Gives the number of photon hitting a dead area or an active area.
  //cout <<"Photon hitting dead area = " << deadcount << ", photon hitting active area = " << goodcount << "out of "<< goodcount+deadcount<< "events"<<endl;
  double tot_count = deadcount+ goodcount;
  double tot_ineff = deadcount_ineff + goodcount_ineff;

  cout << "Good Count " << goodcount << " Bad counts " << deadcount << endl;
  cout << "Counts in the inefficient region: " << ineffcount << " where " << goodcount_ineff << " were considered good, and " << deadcount_ineff << " were considered bad." << endl;
  cout << "This makes total " << tot_ineff << " counts in the inefficient region, which should be equal to " << ineffcount << endl;
  cout << "Achieved fill factor =" <<100* goodcount/tot_count << endl;
  cout << "LY increase =" << ((goodcount/tot_count)/GFF-1)*100 << endl;
  cout << "Of " << ::g_epoxy << " photons arriving at the epoxy " << ::g_epoxy_refl << " are reflected and " << ::g_epoxy_transm << " are transmitted." <<endl;

  //txt file
  char name[255];
  double lens_rad=lens_radius;
  double sphere_rad=sphere_radius;
  sprintf(name,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f,output_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.dat",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
  ofstream results(name);
  results << "For a lens radius of " << lens_radius << "and a sphere radius of "<< sphere_radius << " we get a lens height of " << lens_height << endl;
  results << "Photon hitting dead area = " << deadcount << ", photon hitting active area = " << goodcount << " out of "<< goodcount+deadcount<< " events"<<endl;
  results << "Of " << ::g_epoxy << " photons arriving at the epoxy " << ::g_epoxy_refl << " are reflected and " << ::g_epoxy_transm << " are transmitted." <<endl;
  results << " " << endl;
  results << "Achieved fill factor =" <<100* goodcount/tot_count << endl;
  results << "LY increase =" << ((goodcount/tot_count)/GFF-1)*100 << endl;

    //Lines between pixel
    TLine *l = new TLine(pixel_sizex,0,pixel_sizex,3*pixel_sizey);
    TLine *l1 = new TLine(2*pixel_sizex,0,2*pixel_sizex,3*pixel_sizey);
    TLine *l2 = new TLine(0,pixel_sizey,3*pixel_sizex,pixel_sizey);
    TLine *l3 = new TLine(0,2*pixel_sizey,3*pixel_sizex,2*pixel_sizey);
    TLine *l4 = new TLine(pixel_sizex,0,pixel_sizex,3*pixel_sizey);
    //l->SetLineWidth(2);
    //l1->SetLineWidth(2);
    //l2->SetLineWidth(2);
    //l3->SetLineWidth(2);
    //l4->SetLineWidth(2);

    //dashed line around dead area
    TLine *l_dead1 = new TLine(pixel_sizex-dead_areax,0,pixel_sizex-dead_areax,3*pixel_sizex);
    TLine *l_dead2 = new TLine(pixel_sizex+dead_areax,0,pixel_sizex+dead_areax,3*pixel_sizex);
    TLine *l_dead3 = new TLine(2*pixel_sizex-dead_areax,0,2*pixel_sizex-dead_areax,3*pixel_sizex);
    TLine *l_dead4 = new TLine(2*pixel_sizex+dead_areax,0,2*pixel_sizex+dead_areax,3*pixel_sizex);
    TLine *l_dead5 = new TLine(dead_areax,0,dead_areax,3*pixel_sizex);
    TLine *l_dead6 = new TLine(3*pixel_sizex-dead_areax,0,3*pixel_sizex-dead_areax,3*pixel_sizex);
    TLine *l_dead7 = new TLine(0,dead_areay,3*pixel_sizex,dead_areax);
    TLine *l_dead8 = new TLine(0,pixel_sizey-dead_areay,3*pixel_sizey, pixel_sizey-dead_areay);
    TLine *l_dead9 = new TLine(0,pixel_sizey+dead_areay,3*pixel_sizey, pixel_sizey+dead_areay);
    TLine *l_dead10 = new TLine(0,2*pixel_sizey-dead_areay,3*pixel_sizey,2*pixel_sizey-dead_areay);
    TLine *l_dead11 = new TLine(0,2*pixel_sizey+dead_areay,3*pixel_sizey,2*pixel_sizey+dead_areay);
    TLine *l_dead12 = new TLine(0,3*pixel_sizey-dead_areay,3*pixel_sizey,3*pixel_sizey-dead_areay);
    l_dead12->SetLineStyle(7);
    l_dead11->SetLineStyle(7);
    l_dead10->SetLineStyle(7);
    l_dead9->SetLineStyle(7);
    l_dead8->SetLineStyle(7);
    l_dead7->SetLineStyle(7);
    l_dead6->SetLineStyle(7);
    l_dead5->SetLineStyle(7);
    l_dead4->SetLineStyle(7);
    l_dead3->SetLineStyle(7);
    l_dead2->SetLineStyle(7);
    l_dead1->SetLineStyle(7);
    l_dead12->SetLineWidth(2);
    l_dead11->SetLineWidth(2);
    l_dead10->SetLineWidth(2);
    l_dead9->SetLineWidth(2);
    l_dead8->SetLineWidth(2);
    l_dead7->SetLineWidth(2);
    l_dead6->SetLineWidth(2);
    l_dead5->SetLineWidth(2);
    l_dead4->SetLineWidth(2);
    l_dead3->SetLineWidth(2);
    l_dead2->SetLineWidth(2);
    l_dead1->SetLineWidth(2);
   // l_dead12->SetLineColor(0);
   // l_dead11->SetLineColor(0);
   // l_dead10->SetLineColor(0);
   // l_dead9->SetLineColor(0);
   // l_dead8->SetLineColor(0);
   // l_dead7->SetLineColor(0);
   // l_dead6->SetLineColor(0);
   // l_dead5->SetLineColor(0);
   // l_dead4->SetLineColor(0);
   // l_dead3->SetLineColor(0);
   // l_dead2->SetLineColor(0);
   // l_dead1->SetLineColor(0);

    //dashed line around inefficient region
    TLine *l_ineff1 = new TLine(dead_areax+ineff_areax,0,dead_areax+ineff_areax,3*pixel_sizex);
    TLine *l_ineff2 = new TLine(pixel_sizex-dead_areax-ineff_areax,0,pixel_sizex-dead_areax-ineff_areax,3*pixel_sizex);
    TLine *l_ineff3 = new TLine(pixel_sizex+dead_areax+ineff_areax,0,pixel_sizex+dead_areax+ineff_areax,3*pixel_sizex);
    TLine *l_ineff4 = new TLine(2*pixel_sizex-dead_areax-ineff_areax,0,2*pixel_sizex-dead_areax-ineff_areax,3*pixel_sizex);
    TLine *l_ineff5 = new TLine(2*pixel_sizex+dead_areax+ineff_areax,0,2*pixel_sizex+dead_areax+ineff_areax,3*pixel_sizex);
    TLine *l_ineff6 = new TLine(3*pixel_sizex-dead_areax-ineff_areax,0,3*pixel_sizex-dead_areax-ineff_areax,3*pixel_sizex);
    TLine *l_ineff7 = new TLine(0,dead_areay+ineff_areay,3*pixel_sizex,dead_areay+ineff_areay);
    TLine *l_ineff8 = new TLine(0,pixel_sizey-dead_areay-ineff_areay,3*pixel_sizex,pixel_sizey-dead_areay-ineff_areay);
    TLine *l_ineff9 = new TLine(0,pixel_sizey+dead_areay+ineff_areay,3*pixel_sizex,pixel_sizey+dead_areay+ineff_areay);
    TLine *l_ineff10 = new TLine(0,2*pixel_sizey-dead_areay-ineff_areay,3*pixel_sizex,2*pixel_sizey-dead_areay-ineff_areay);
    TLine *l_ineff11 = new TLine(0,2*pixel_sizey+dead_areay+ineff_areay,3*pixel_sizex,2*pixel_sizey+dead_areay+ineff_areay);
    TLine *l_ineff12 = new TLine(0,3*pixel_sizey-dead_areay-ineff_areay,3*pixel_sizex,3*pixel_sizey-dead_areay-ineff_areay);
    l_ineff1->SetLineStyle(8);
    l_ineff2->SetLineStyle(8);
    l_ineff3->SetLineStyle(8);
    l_ineff4->SetLineStyle(8);
    l_ineff5->SetLineStyle(8);
    l_ineff6->SetLineStyle(8);
    l_ineff7->SetLineStyle(8);
    l_ineff8->SetLineStyle(8);
    l_ineff9->SetLineStyle(8);
    l_ineff10->SetLineStyle(8);
    l_ineff11->SetLineStyle(8);
    l_ineff12->SetLineStyle(8);
    //l_ineff1->SetLineWidth(2);
    //l_ineff2->SetLineWidth(2);
    //l_ineff3->SetLineWidth(2);
    //l_ineff4->SetLineWidth(2);
    //l_ineff5->SetLineWidth(2);
    //l_ineff6->SetLineWidth(2);
    //l_ineff7->SetLineWidth(2);
    //l_ineff8->SetLineWidth(2);
    //l_ineff9->SetLineWidth(2);
    //l_ineff10->SetLineWidth(2);
    //l_ineff11->SetLineWidth(2);
    //l_ineff12->SetLineWidth(2);
    //l_ineff1->SetLineColor(4);
    //l_ineff2->SetLineColor(4);
    //l_ineff3->SetLineColor(4);
    //l_ineff4->SetLineColor(4);
    //l_ineff5->SetLineColor(4);
    //l_ineff6->SetLineColor(4);
    //l_ineff7->SetLineColor(4);
    //l_ineff8->SetLineColor(4);
    //l_ineff9->SetLineColor(4);
    //l_ineff10->SetLineColor(4);
    //l_ineff11->SetLineColor(4);
    //l_ineff12->SetLineColor(4);
  
    c1->cd();
    Photon_generation->Draw("colz");
    Photon_generation->GetXaxis()->SetTitle("x[#mum]");
    Photon_generation->GetYaxis()->SetTitle("y[#mum]");
    char namec1[255];
    sprintf(namec1,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Photongeneration_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c1->SaveAs(namec1);

    c2->cd();
    g->SetMarkerStyle(20);
    g->Draw("pcol");
    g->GetXaxis()->SetRangeUser(-50,210);
    g->GetYaxis()->SetRangeUser(-50,210); 

    c3->cd();
    Lens->Draw("colz");
    Lens->GetXaxis()->SetTitle("x[#mum]");
    Lens->GetYaxis()->SetTitle("y[#mum]");
    Lens->SetLineWidth(2);
    l->Draw();
    l1->Draw();
    l2->Draw();
    l3->Draw();
    l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
    l_ineff1->Draw();
    l_ineff2->Draw();
    l_ineff3->Draw();
    l_ineff4->Draw();
    l_ineff5->Draw();
    l_ineff6->Draw();
    l_ineff7->Draw();
    l_ineff8->Draw();
    l_ineff9->Draw();
    l_ineff10->Draw();
    l_ineff11->Draw();
    l_ineff12->Draw();
    char namec3[255];
    sprintf(namec3,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Lens_effect_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c3->SaveAs(namec3);

    c9->cd();
    Lens2->Draw("colz");
    Lens2->GetXaxis()->SetTitle("x[#mum]");
    Lens2->GetYaxis()->SetTitle("y[#mum]");
    Lens2->SetLineWidth(2);
    l->Draw();
    l1->Draw();
    l2->Draw();
    l3->Draw();
    l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
    l_ineff1->Draw();
    l_ineff2->Draw();
    l_ineff3->Draw();
    l_ineff4->Draw();
    l_ineff5->Draw();
    l_ineff6->Draw();
    l_ineff7->Draw();
    l_ineff8->Draw();
    l_ineff9->Draw();
    l_ineff10->Draw();
    l_ineff11->Draw();
    l_ineff12->Draw();
    char namec9[255];
    sprintf(namec9,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Lens2_effect_Lens2_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c9->SaveAs(namec9);
    
    c10->cd();
    Lens2Proj->Draw("bar");
    //Lens2Proj->GetYaxis()->SetRange(0)
    char namec10[255];
    sprintf(namec10,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Proj_Lens2_effect_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c10->SaveAs(namec10);

    c4->cd();
    bad->Draw("colz"); 
    bad->GetXaxis()->SetTitle("x[#mum]");
    bad->GetYaxis()->SetTitle("y[#mum]");
    bad->SetLineWidth(2);
    l->Draw();
    l1->Draw();
    l2->Draw();
    l3->Draw();
    l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
    l_ineff1->Draw();
    l_ineff2->Draw();
    l_ineff3->Draw();
    l_ineff4->Draw();
    l_ineff5->Draw();
    l_ineff6->Draw();
    l_ineff7->Draw();
    l_ineff8->Draw();
    l_ineff9->Draw();
    l_ineff10->Draw();
    l_ineff11->Draw();
    l_ineff12->Draw();
    char namec4[255];
    sprintf(namec4,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Dead_area_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c4->SaveAs(namec4);

    c5->cd();
    good->Draw("colz");
    good->GetXaxis()->SetTitle("x[#mum]");
    good->GetYaxis()->SetTitle("y[#mum]");
    good->SetLineWidth(2);
    l->Draw();
    l1->Draw();
    l2->Draw();
    l3->Draw();
    l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
    l_ineff1->Draw();
    l_ineff2->Draw();
    l_ineff3->Draw();
    l_ineff4->Draw();
    l_ineff5->Draw();
    l_ineff6->Draw();
    l_ineff7->Draw();
    l_ineff8->Draw();
    l_ineff9->Draw();
    l_ineff10->Draw();
    l_ineff11->Draw();
    l_ineff12->Draw();
    char namec5[255];
    sprintf(namec5,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Active_area_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c5->SaveAs(namec5);

    c6->cd();
    phi->Draw();
    good->GetXaxis()->SetTitle("x[#mum]");
    good->GetYaxis()->SetTitle("y[#mum]");

    c7->cd();
    Double_t scale_phiprop = phiprop->GetEntries();
    Double_t scale_phi = phi->GetEntries();
    phiprop->Draw();
    phiprop->Scale(1.0/scale_phiprop);
    phi->SetLineColor(kRed);
    phi->Draw("same");
    phi->Scale(1.0/scale_phi);
    char namec7[255];
    sprintf(namec7,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_AngularDist_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c7->SaveAs(namec7);


    //c8->cd();
   // bad2->Draw("colz"); 
   // bad2->GetXaxis()->SetTitle("x[#mum]");
   // bad2->GetYaxis()->SetTitle("y[#mum]");
   // good->SetLineWidth(2);
   // l->Draw();
   // l1->Draw();
   // l2->Draw();
   // l3->Draw();
   // l4->Draw();
   // l_dead12->Draw();
   // l_dead11->Draw();
   // l_dead10->Draw();
   // l_dead9->Draw();
   // l_dead8->Draw();
   // l_dead7->Draw();
   // l_dead6->Draw();
   // l_dead5->Draw();
   // l_dead4->Draw();
   // l_dead3->Draw();
   // l_dead2->Draw();
   // l_dead1->Draw();
   // char namec8[255];
   // sprintf(namec8,"/home/lphe/Lens/Lens_Carina/Results/Dead_Area2_Lens_radius%f_Sphere_radius%f.root",lens_rad,sphere_rad);
   // c8->SaveAs(namec8);

    c8->cd();
    photongeneration_proj->Draw("bar");
    char namec8[255];
    sprintf(namec8,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Proj_photongeneration_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c8->SaveAs(namec8);

    c11->cd();
    //badProj->Draw("bar");
    photongeneration_proj->Draw("");
    char namec11[255];
    sprintf(namec11,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Proj_dead_area_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c11->SaveAs(namec11);

    c12->cd();
    //goodProj->Draw("bar");
    photongeneration_proj->Draw("B");
    char namec12[255];
    sprintf(namec12,"/Users/trippl/Documents/EPFL/SiPM/Microlenses/Simulation/Git/microlenssimulation/GFF%f_Lens%f_Proj_active_area_Lens_radius%f_Sphere_radius%f_Hres%f_efficiency%f.root",GFF,lens_status,lens_rad,sphere_rad,Hrect,ineff_efficiency);
    c12->SaveAs(namec12);

    return 0;
}





#include <iostream>
#include <TH2.h>
#include <TH2D.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <random>
#include <chrono>
#include "TMath.h"
#include <cmath>
#include <math.h> 
#include <TLine.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <THStack.h>
#include <stdio.h>
#include <fstream>

using namespace std;
//Pixel size in x
#define pixel_sizex 40
//Pixel size in y
#define pixel_sizey 40

//global counters to count photons arriving at epoxy, number of photons reflected/transmitted at epoxy
int g_epoxy=0;
int g_epoxy_refl=0;
int g_epoxy_transm=0;

//Definition of the producte between a scalar and a vector
vector<double> Prod(double u, vector<double> v){

  vector<double> prod_vect;
  
  prod_vect.push_back(u*v[0]);
  prod_vect.push_back(u*v[1]);
  prod_vect.push_back(u*v[2]);
  return prod_vect;
}

//Definition of the vector product
vector<double> Prod_vect(vector<double>u, vector<double> v){

  vector<double> prod_vect;
  
  prod_vect.push_back(u[1]*v[2]-u[2]*v[1]);
  prod_vect.push_back(u[2]*v[0]-u[0]*v[2]);
  prod_vect.push_back(u[0]*v[1]-u[1]*v[0]);
  return prod_vect;
}

//Definition of the scalar product
double Prod_scal(vector<double>u, vector<double> v){

  double prod_scal= 0.0;
 
  prod_scal=u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
  return prod_scal;
}

//Schlick's approximation for the probability of reflection.
double Schlick_Reflection(double angle, double n1, double n2){
  double prob;
  double R0=((n1-n2)/(n1+n2));
  prob = pow(R0,2)+(1-pow(R0,2))*pow((1-cos(angle)),5);
  return prob;
}

//Angular distribution of the angle between z and the x-y plane at the output of the fibre.
//Linear between 0° and45.5 °. Exponentialy decreasing between45.6° and 90°.
//Angular output of fibre in air -> experimental values from measurement in lab. 
//Commented values are from old simulation, probably angular distribution in fibre (non-educated guess Carina)
double distributionOfTheAngle(double x){
  if(x<0.0) {
    return 0.0;
  }
  //else if(x<32){
    //return 0.0+0.025*(x);
  //}
  else if (x<=45.5){
    return 0.0344*x + 0.0655;
  }
  //else if ( x>32 && x <=90){
    //return 0.0150496 + 34.3143*TMath::Exp(-x* 0.109388);
  //}
  else if (x>45.5 && x<=90){
    return 177.72*TMath::Exp(-0.111*x);
  }
  else return 0.0;
}

//Generate a random angle following the distribution given in "distributionOfTheAngle
double getRandomAngleOfEmission(double angle_cut){  

  TF1* func = new TF1("Func","distributionOfTheAngle(x)",0,angle_cut); 
  double alpha=func->GetRandom();
  delete(func);   
  //Transformation in radian
  return alpha*M_PI/180;
} 

//Function that generates the photon, random between pixel centre - generation area and pixel centre + generation area
vector<double> photon_generation(double generation_areax, double generation_areay,double height)
{
  vector<double> photon;
  double posx=0.0;
  double posy=0.0;
  double theta=0.0;
  double phi=0.0;

  //Randomly genetarated theta between 0 and 2PI. Theta gives the angle in the x-y plane
  std::uniform_real_distribution<double> theta_(0,2*M_PI);
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator_angle (seed);
  theta=theta_(generator_angle);
  //Generation of phi following the distribution given in "distributionOfTheAngle". 
  //The angle cut is given in the argument of the function
  phi=getRandomAngleOfEmission(90);

  //Generation of the position of the photon
  std::uniform_real_distribution<double> unifx(3*pixel_sizex/2-generation_areax,3*pixel_sizex/2+generation_areax);
  std::uniform_real_distribution<double> unify(3*pixel_sizey/2-generation_areay,3*pixel_sizey/2+generation_areay);
  unsigned seed2 = std::chrono::system_clock::now().time_since_epoch().count();
  std::random_device rd;
  std::mt19937 gen(rd());
  std::default_random_engine generator (seed2);
  posx= unifx(gen);
  unsigned seed3 = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator2 (seed3);
  posy= unify(generator2);

  //The return vector has the following structure : 1) position in x 2) position in y 3) height of the generation 4) theta [0°-360°] 5) phi [0°-90°]
  photon.push_back(posx);
  photon.push_back(posy);
  photon.push_back(height);
  photon.push_back(theta);
  photon.push_back(phi);
  return photon;
}

//Give the position of the photon at different heigths
vector<double> GetPhotonAtPosition(double posx, double posy, double angle_azy, double angle_pol, double height)
{     
  vector<double> position_inheight;
  posx=posx+cos(angle_azy)*tan(angle_pol)*height;
  posy=posy+sin(angle_azy)*tan(angle_pol)*height;
  position_inheight.push_back(posx);
  position_inheight.push_back(posy);
  return position_inheight;
}

//Function that gives the interstion of the photon with the lense. Distinguish between the case where the photon hits the lens or not.
vector<double> Photon_intersection(double posx, double posy, double theta, double phi, double height, double lens_radius, double lens_height, int only_lens){
    vector<double>intersection;
    double x_int=0.0;
    double y_int=0.0;
    double z_int=0.0;
    double rho=0.0;
    //Position of the center of the sphere in the photon generation referential. The sphere is center in x ((3*pixel_sizex/2)um) and y ((3*pixel_sizey/2)um) for pixel
    //size of 50um in x and 50um in y.Distance z is tunable with the sphere radius and the lens height. !! Center of the sphere can be in z<0
    // but only the interaction point with z>0 are taken into account!!. With these two parameters the lens radius and the lens height are tunable.
    double x_ = 3*pixel_sizex/2-posx;
    double y_ = 3*pixel_sizey/2-posy;
    //double z_ = height+(sphere_radius-lens_height); //SPHERE
    double z_ = height-lens_height;
    double d = (pow(lens_radius,2)/lens_height);
    // //Computation of the solution of the intersection between a vector and a paraboloid with the referential at the photon generation point
    double a = pow(sin(phi),2);
    double b = -2*sin(phi)*cos(theta)*x_-2*sin(phi)*sin(theta)*y_-cos(phi)*d;
    double c = pow(x_,2)+pow(y_,2)-z_*d;
    
    rho = (-b-sqrt(pow(b,2)-4*a*c))/(2*a);

    //x,y,z are then re-expressed in the referential set at the begining of the first pixel
    x_int=rho*sin(phi)*cos(theta)+posx;
    y_int=rho*sin(phi)*sin(theta)+posy;
    //z_int=lens_height-rho*cos(phi);
    z_int=height-rho*cos(phi);
    
    //Case where interaction point is below z<0, above z>lens_height or that have no intersection with the paraboloid
    if(rho != rho || z_int<0){
      //Select all event
      if (only_lens==0){
	       intersection.push_back(posx+cos(theta)*tan(phi)*height);
	       intersection.push_back(posy+sin(theta)*tan(phi)*height);
      }
      //Select only event that have interacted with the sphere
      if (only_lens==1){
	       intersection.push_back(-1.0);
	       intersection.push_back(-1.0);
      }
      intersection.push_back(0.0);
    }
    //Event that have intersected
    else{  
        intersection.push_back(x_int);
        intersection.push_back(y_int);
        intersection.push_back(z_int);
    }
    intersection.push_back(theta);
    intersection.push_back(phi);
    return intersection;
  }
  
//Function that look at the photon propagation. Two cases : 1) Lens is deactivated (lens_status=0) 2) Lens is activated (lens_status=1) 
vector<double> Photon_propagation(double posx, double posy, double theta, double phi, double height, double lens_radius,double lens_height, int lens_status, int only_lens)
{
  //DEACTIVATED: Case where the lens is deactivated
  vector<double> Final_position;
  if( lens_status == 0 ){
    posx=posx+cos(theta)*tan(phi)*height;
    posy=posy+sin(theta)*tan(phi)*height;
    Final_position.push_back(posx);
    Final_position.push_back(posy);
    //cout << " Theta in lens status 0" << theta << endl;
    //cout << " Phi in lens status 0" << phi << endl;
    Final_position.push_back(theta);//AM
    Final_position.push_back(phi);//AM
    return Final_position; //AM: returns x,y,theta,phi below lens
    }

  //ACTIVATED: Case where the lens is activated  
  else{

    //Get the intersection between the photon and the lens
    vector<double> Intersection;
    Intersection= Photon_intersection(posx,  posy,  theta,  phi,  height,  lens_radius,lens_height,only_lens);
    //cout<<"intersection after: "<<Intersection[0]<<" "<<Intersection[1]<<" "<<Intersection[2]<<" "<<Intersection[3]<<endl;
    //If no interaction is seen (given if Intersection[2] ( height) is given by 0), nothing more needs to be done
    if(Intersection[2]==0){
      vector<double> interm;
      interm.push_back(Intersection[0]);
      interm.push_back(Intersection[1]);
      interm.push_back(Intersection[3]);
      interm.push_back(Intersection[4]);

      return interm;}
    
    //Propagation of the photon that has interact with the lense. The photon is either transmitted or reflected, depending of the angle with the normal. Propability given by
    //Sclick's approximation
    else{
      //Definition of the normal to a sphere at a point (n=gradf(p), with f is the equation of a sphere and p the point where the normal is search)
      vector<double> normal;

      double d = pow(lens_radius,2)/lens_height;
      normal.push_back(2*(Intersection[0]-3*pixel_sizex/2));
      normal.push_back(2*(Intersection[1]-3*pixel_sizey/2));
      normal.push_back(d);
      //Paraboloid equation (x-x0)^2+(y-y0)^2=-(z-z0)*d

      //Normalisation of the normal
      double norm_normal = sqrt(pow(normal[0],2)+pow(normal[1],2)+pow(normal[2],2));
      normal[0]=normal[0]/norm_normal;
      normal[1]=normal[1]/norm_normal;
      normal[2]=normal[2]/norm_normal;

      //Vector of the photon at the interaction point
      vector<double> photon_vector;
      photon_vector.push_back(Intersection[0]-posx);
      photon_vector.push_back(Intersection[1]-posy);
      photon_vector.push_back(Intersection[2]-height);
      
      //Normalisation of the photon vector
      double norm_photon_vector = sqrt(pow(photon_vector[0],2)+pow(photon_vector[1],2)+pow(photon_vector[2],2));
      photon_vector[0]=photon_vector[0]/norm_photon_vector;
      photon_vector[1]=photon_vector[1]/norm_photon_vector;
      photon_vector[2]=photon_vector[2]/norm_photon_vector;
    
      //Computation of the angle between the incident photon and the normal on the sphere at that point. This angle is then used for the Schlick's reflection probability
      double alpha_i =0.0;
      double prod_scal=normal[0]*photon_vector[0]+normal[1]*photon_vector[1]+normal[2]*photon_vector[2];
      alpha_i = acos(prod_scal/(norm_normal*norm_photon_vector));
     
      double proba=0.0;
      //Compute Schlick's reflection probability for the angle
      proba=Schlick_Reflection(alpha_i, 1., 1.5);
      proba=proba/100.0;
      
      //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<> dis(0, 1);
      double gene_prob = dis(gen);
      
      //REFLECTION: Case where the photon is reflected
      if (gene_prob<proba){
        vector<double> reflected;
        vector<double> part;
          //cout<<"reflection in action"<<endl;
        //Direction vector of the reflected photon
        part=Prod(2*Prod_scal(photon_vector,normal),normal);
        
        reflected.push_back(photon_vector[0]-part[0]);
        reflected.push_back(photon_vector[1]-part[1]);
        reflected.push_back(photon_vector[2]-part[2]);

        //If the direction vector of the reflected photon is negativ, it means that it can still be detected. Point of interaction is given by the intersection of a vector (line) and a plan
        //normal (0,0,1).
        if (reflected[2]<0){
           Final_position.push_back(-1000);
           Final_position.push_back(-1000);
           Final_position.push_back(-1000);
           Final_position.push_back(-1000);
	       return Final_position;
        }
        //Lost photon
        else {
	       Final_position.push_back(-1000);
	       Final_position.push_back(-1000);
	       Final_position.push_back(-1000);
           Final_position.push_back(-1000);
	       return Final_position;
        }
     }
      //TRANSMISSION: Case where the photon is transmitted. The transmitted vector is given by Snell's law in 3D (vectorial form)
      else{
        vector<double> transmitted;

        vector<double> first_part;
        first_part = Prod((1.0/1.5),Prod_vect(normal,Prod_vect(Prod(-1,normal),photon_vector)));
        double p = 0.0;
        p = Prod_scal(Prod_vect(normal,photon_vector),Prod_vect(normal,photon_vector));
        vector<double> second_part;
        second_part = Prod(sqrt(1-(pow((1/1.5),2)*p)),normal);

        transmitted.push_back(first_part[0]-second_part[0]);
        transmitted.push_back(first_part[1]-second_part[1]);
        transmitted.push_back(first_part[2]-second_part[2]);

        
        // Point of interaction is given by the intersection of a vector (line) and a plan
        double positionx=-(Intersection[2]/transmitted[2])*transmitted[0]+Intersection[0];
        double positiony=-(Intersection[2]/transmitted[2])*transmitted[1]+Intersection[1];
        double positionz=-(Intersection[2]/transmitted[2])*transmitted[2]+Intersection[2];
      
        Final_position.push_back(positionx);
        Final_position.push_back(positiony);
        
        double angle_phi = acos(transmitted[2]/sqrt(Prod_scal(transmitted,transmitted)));
        //cout << "Theta at the source" << (M_PI-angle_theta) << endl;
        //FAUT VERIFIER PHI
        double angle_theta = atan2(transmitted[1],transmitted[0]);
        //double angle_theta = atan(transmitted[1]/transmitted[0]);
        //cout << "Phi at the source" << angle_phi*180/M_PI << endl;

        Final_position.push_back(angle_theta);//(M_PI-angle_theta);
        Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
        return Final_position;
      }
    }
  }
}

vector<double> rectunder(double x, double y, double xstart, double ystart, double h, double heightabs){
    
    vector<double> Final_position;
    
    vector<double> normal;
    normal.push_back(0);
    normal.push_back(0);
    normal.push_back(1);
    
    //Normalisation of the normal
    double norm_normal = sqrt(pow(normal[0],2)+pow(normal[1],2)+pow(normal[2],2));
    normal[0]=normal[0]/norm_normal;
    normal[1]=normal[1]/norm_normal;
    normal[2]=normal[2]/norm_normal;
    
    //Vector of the photon at the interaction point
    vector<double> photon_vector;
    photon_vector.push_back(x-xstart);
    photon_vector.push_back(y-ystart);
    photon_vector.push_back(-heightabs);
    //cout<<"rect initial x "<<xstart<<endl;
    //cout<<"rect initial y "<<ystart<<endl;
    //cout<<"rect initial z "<<heightabs<<endl;
    //Normalisation of the photon vector
    double norm_photon_vector = sqrt(pow(photon_vector[0],2)+pow(photon_vector[1],2)+pow(photon_vector[2],2));
    photon_vector[0]=photon_vector[0]/norm_photon_vector;
    photon_vector[1]=photon_vector[1]/norm_photon_vector;
    photon_vector[2]=photon_vector[2]/norm_photon_vector;
    
    
    //Computation of the angle between the incident photon and the normal on the sphere at that point. This angle is then used for the Schlick's reflection probability
    double alpha_i =0.0;
    double prod_scal=normal[0]*photon_vector[0]+normal[1]*photon_vector[1]+normal[2]*photon_vector[2];
    alpha_i = acos(prod_scal/(norm_normal*norm_photon_vector));
    
    double proba=0.0;
    //Compute Schlick's reflection probability for the angle
    proba=Schlick_Reflection(alpha_i, 1.5, 1.5);
    proba=proba/100.0;
    
    //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    double gene_prob = dis(gen);
    

    ::g_epoxy+=1;

    //REFLECTION: Case where the photon is reflected
    if (gene_prob<proba){
        
        ::g_epoxy_refl+=1;

        vector<double> reflected;
        vector<double> part;
        
        //Direction vector of the reflected photon
        part=Prod(2*Prod_scal(photon_vector,normal),normal);
        
        reflected.push_back(photon_vector[0]-part[0]);
        reflected.push_back(photon_vector[1]-part[1]);
        reflected.push_back(photon_vector[2]-part[2]);
        
        //If the direction vector of the reflected photon is negative, it means that it can still be detected. Point of interaction is given by the intersection of a vector (line) and a plan
        //normal (0,0,1).
        if (reflected[2]<0){
            //cout<<"rect reflected"<<endl;
            double positionx=-1000;
            double positiony=-1000;
            double positionz=-1000;
            Final_position.push_back(positionx);
            Final_position.push_back(positiony);
            
            double angle_phi = acos(reflected[2]/sqrt(Prod_scal(reflected,reflected)));
            double angle_theta = atan2(reflected[1],reflected[0]);
            //double angle_theta = atan(reflected[1]/reflected[0]);
            Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
            Final_position.push_back(angle_theta);//(M_PI-angle_theta);
            
            return Final_position;
        }
        //Lost photon
        else {
            //cout<<"rect lost"<<endl;
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            return Final_position;
        }
    }
    //TRANSMISSION: Case where the photon is transmitted. The transmitted vector is given by Snell's law in 3D (vectorial form)
    else{
        
        ::g_epoxy_transm+=1;
        
        vector<double> transmitted;
        //cout<<"rect transmitted"<<endl;
        vector<double> first_part;
        first_part = Prod((1.5/1.5),Prod_vect(normal,Prod_vect(Prod(-1,normal),photon_vector)));
        double p = 0.0;
        p = Prod_scal(Prod_vect(normal,photon_vector),Prod_vect(normal,photon_vector));
        vector<double> second_part;
        second_part = Prod(sqrt(1-(pow((1.5/1.5),2)*p)),normal);
        
        transmitted.push_back(first_part[0]-second_part[0]);
        transmitted.push_back(first_part[1]-second_part[1]);
        transmitted.push_back(first_part[2]-second_part[2]);
        
        
        // Point of interaction is given by the intersection of a vector (line) and a plan
        double positionx=-(h/transmitted[2])*transmitted[0]+x;
        double positiony=-(h/transmitted[2])*transmitted[1]+y;
        double positionz=-(h/transmitted[2])*transmitted[2];
        
        //cout<<"rect posx "<<positionx<<endl;
        //cout<<"rect posy "<<positiony<<endl;
        //cout<<"rect posz "<<positionz<<endl;
        
        Final_position.push_back(positionx);
        Final_position.push_back(positiony);
        
        double angle_phi = acos(transmitted[2]/sqrt(Prod_scal(transmitted,transmitted)));
        double angle_theta = atan2(transmitted[1],transmitted[0]);
        //cout << "Theta at the source" << (M_PI-angle_theta) << endl;
        //double angle_theta = atan(transmitted[1]/transmitted[0]);
        //cout << "Phi at the source" << angle_phi*180/M_PI << endl;
        
        Final_position.push_back(angle_theta);//(M_PI-angle_theta);
        Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
        return Final_position;
    }
    
}

//AM: Adding a rectangular piece to the lense
//input (describing the position and direction of a photon on the initial plane): vector<double>: x,y,theta [0,pi/2), phi [0,2pi), h
//output (intersection between photon and plane translated in z direction by -h): vector<double>: xn, yn
vector<double> rect(double x, double y, double xstart, double ystart, double h, double heightabs){
    
    vector<double> Final_position;
    
    vector<double> normal;
    normal.push_back(0);
    normal.push_back(0);
    normal.push_back(1);
    
    //Normalisation of the normal
    double norm_normal = sqrt(pow(normal[0],2)+pow(normal[1],2)+pow(normal[2],2));
    normal[0]=normal[0]/norm_normal;
    normal[1]=normal[1]/norm_normal;
    normal[2]=normal[2]/norm_normal;
    
    //Vector of the photon at the interaction point
    vector<double> photon_vector;
    photon_vector.push_back(x-xstart);
    photon_vector.push_back(y-ystart);
    photon_vector.push_back(-heightabs);
    //cout<<"rect initial x "<<xstart<<endl;
    //cout<<"rect initial y "<<ystart<<endl;
    //cout<<"rect initial z "<<heightabs<<endl;
    //Normalisation of the photon vector
    double norm_photon_vector = sqrt(pow(photon_vector[0],2)+pow(photon_vector[1],2)+pow(photon_vector[2],2));
    photon_vector[0]=photon_vector[0]/norm_photon_vector;
    photon_vector[1]=photon_vector[1]/norm_photon_vector;
    photon_vector[2]=photon_vector[2]/norm_photon_vector;
    
    
    //Computation of the angle between the incident photon and the normal on the sphere at that point. This angle is then used for the Schlick's reflection probability
    double alpha_i =0.0;
    double prod_scal=normal[0]*photon_vector[0]+normal[1]*photon_vector[1]+normal[2]*photon_vector[2];
    alpha_i = acos(prod_scal/(norm_normal*norm_photon_vector));
    
    double proba=0.0;
    //Compute Schlick's reflection probability for the angle
    proba=Schlick_Reflection(alpha_i,1.,1.5);
    proba=proba/100.0;
    
    //Generate a random number that is used to compare to "proba" that will determined it the photon is reflected or transmitted
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    double gene_prob = dis(gen);
    
    ::g_epoxy+=1;

    //REFLECTION: Case where the photon is reflected
    if (gene_prob<proba){
        
        ::g_epoxy_refl+=1;
        
        vector<double> reflected;
        vector<double> part;
        
        //Direction vector of the reflected photon
        part=Prod(2*Prod_scal(photon_vector,normal),normal);
        
        reflected.push_back(photon_vector[0]-part[0]);
        reflected.push_back(photon_vector[1]-part[1]);
        reflected.push_back(photon_vector[2]-part[2]);
        
        //If the direction vector of the reflected photon is negative, it means that it can still be detected. Point of interaction is given by the intersection of a vector (line) and a plan
        //normal (0,0,1).
        if (reflected[2]<0){
            //cout<<"rect reflected"<<endl;
            double positionx=-1000;
            double positiony=-1000;
            double positionz=-1000;
            Final_position.push_back(positionx);
            Final_position.push_back(positiony);
            
            double angle_phi = acos(reflected[2]/sqrt(Prod_scal(reflected,reflected)));
            double angle_theta = atan2(reflected[1],reflected[0]);
            //double angle_theta = atan(reflected[1]/reflected[0]);
            Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
            Final_position.push_back(angle_theta);//(M_PI-angle_theta);
            
            return Final_position;
        }
        //Lost photon
        else {
            //cout<<"rect lost"<<endl;
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            Final_position.push_back(-1000);
            return Final_position;
        }
    }
    //TRANSMISSION: Case where the photon is transmitted. The transmitted vector is given by Snell's law in 3D (vectorial form)
    else{

        ::g_epoxy_transm+=1;

        vector<double> transmitted;
        //cout<<"rect transmitted"<<endl;
        vector<double> first_part;
        first_part = Prod((1.0/1.5),Prod_vect(normal,Prod_vect(Prod(-1,normal),photon_vector)));
        double p = 0.0;
        p = Prod_scal(Prod_vect(normal,photon_vector),Prod_vect(normal,photon_vector));
        vector<double> second_part;
        second_part = Prod(sqrt(1-(pow((1/1.5),2)*p)),normal);
        
        transmitted.push_back(first_part[0]-second_part[0]);
        transmitted.push_back(first_part[1]-second_part[1]);
        transmitted.push_back(first_part[2]-second_part[2]);
        
        
        // Point of interaction is given by the intersection of a vector (line) and a plan
        double positionx=-(h/transmitted[2])*transmitted[0]+x;
        double positiony=-(h/transmitted[2])*transmitted[1]+y;
        double positionz=-(h/transmitted[2])*transmitted[2];
        
        //cout<<"rect posx "<<positionx<<endl;
        //cout<<"rect posy "<<positiony<<endl;
        //cout<<"rect posz "<<positionz<<endl;
        
        Final_position.push_back(positionx);
        Final_position.push_back(positiony);
        
        double angle_phi = acos(transmitted[2]/sqrt(Prod_scal(transmitted,transmitted)));
        //cout << "Theta at the source" << (M_PI-angle_theta) << endl;
        double angle_theta = atan2(transmitted[1],transmitted[0]);
        //double angle_theta = atan(transmitted[1]/transmitted[0]);
        //cout << "Phi at the source" << angle_phi*180/M_PI << endl;
        
        Final_position.push_back(angle_theta);//(M_PI-angle_theta);
        Final_position.push_back(M_PI-angle_phi);//(angle_phi);//acos(reflected[0]/(pow(reflected[1],2)+pow(reflected[0],2))));
        return Final_position;
    }
    
}
 
//Main of the program
int Lens_para(int event,double lens_radius, double lens_height, double Hrect)
{
  //int event => number of photons generated
  //lens_radius => radius of lens ... 
  //lens_height => height of lens ...
  //Hrect => heiht of epoxy
  
  //Geometrical fill factor
  double GFF = 0.8;

  //leave sphere_radius still here in case it is used somewhere
  //double sphere_radius=lens_radius;
  
  //The height (z position of the generated photon) is given by the lens_height + 1um
  double distance = lens_height+0.01;
  double height = distance;
  cout<<"height: "<<height<<endl;

  //Dead area computation
  double dead_areax = pixel_sizex*((1-sqrt(GFF))/2);
  double dead_areay = pixel_sizey*((1-sqrt(GFF))/2);

  //Status of the lens and the non-interactive photon
  double lens_status = 1.; //1=on,0=off
  double only_lens = 0.;// 1 = only lens, 0 = all event

  //Hh= height of pixel-hole
  //Lens might not sit on Silicon directly; could sit on resistor or wires
  double Hh=0.;//=7.;
  
  vector<double> distribution;
  vector<double> propagation;

  //Count of photon hitting dead area or active area
  int deadcount=0;
  int goodcount=0;

  int cphi=0;
  int cphiprop=0;
  
  vector<int> list_of_out;
  vector<double> intersection;

  //Canvas lists
  TCanvas *c1 = new TCanvas("c1","c1",700,700);
  TCanvas *c2 = new TCanvas("c2","c2",700,700);
  TCanvas *c3 = new TCanvas("c3","c3",700,700);
  TCanvas *c4 = new TCanvas("c4","c4",700,700);
  TCanvas *c5 = new TCanvas("c5","c5",700,700);
  TCanvas *c6 = new TCanvas("c6","c6",700,700);
  TCanvas *c7 = new TCanvas("c7","c7",700,700);
  TCanvas *c8 = new TCanvas("c8","c8",700,700);
  TCanvas *c9 = new TCanvas("c9","c9",700,700);
  TCanvas *c10 = new TCanvas("c10","c10",700,700);
  TCanvas *c11 = new TCanvas("c11","c11",700,700);
  TCanvas *c12 = new TCanvas("c12","c12",700,700);
  //Plot of the generation point of the photons
  TH2D *Photon_generation=new TH2D("photon generaton","generation of photons",100,0,3*pixel_sizex,100,0,3*pixel_sizey);
  //Plot of the interaction point on the lens
  TGraph2D *g = new TGraph2D();
  TGraph *Projg = new TGraph();
  //Hit position on the detector after the interaction with the lens
  TH2D *Lens=new TH2D("Lens effect","Lens effect",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
   //Hit position on the detector after the interaction with the lens
  TH2D *Lens2=new TH2D("Lenseffect2","Photon distribution (after propagation)",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *Lens2Proj=new TH1D("Lenseffect2 projection", "Projection Photon distribution (after propagation)", 3*pixel_sizex,0,3*pixel_sizex);
  //Hit position on the dead area
  TH2D *bad=new TH2D("bad","Dead Area Hit",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  //Hit position in the active area
  TH2D *good=new TH2D("good","Active Area Hit",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *goodProj=new TH1D("good projection", "Projection Active area hits", 3*pixel_sizex,0,3*pixel_sizex);
  //Angular distribution
  TH1D *phi=new TH1D("phidistri","Angular distribution",90,0,90);
  //Angular distribution after propagatoin
  TH1D *phiprop=new TH1D("phipdistri","Angular distribution",90,0,90);
  TH2D *bad2=new TH2D("bad2","Dead Area2 Hit",3*pixel_sizex,0,3*pixel_sizex,3*pixel_sizey,0,3*pixel_sizey);
  TH1D *badProj=new TH1D("bad projection", "Projection Dead area hits", 3*pixel_sizex,0,3*pixel_sizex);

  for (int i =0; i< event; ++i){
    if(i%100000==0)cout << " Event Nbr " << i << endl;

    //Generate the photon distribution (middle of array +- generation area)
    distribution = photon_generation(200,200,distance);
    //Fill the histogram of the photon generation
    //cout << "x initial " << distribution[0] << endl;
    //cout << "y initial " << distribution[1] << endl;
    Photon_generation->Fill(distribution[0], distribution[1]);

    //Intersection computation
    intersection = Photon_intersection(distribution[0],distribution[1], distribution[3], distribution[4], distribution[2], lens_radius,lens_height, only_lens);
    //cout<<"intersection before: "<<intersection[0]<<" "<<intersection[1]<<" "<<intersection[2]<<" "<<intersection[3]<<endl;
    
    //Propagation of the photons
    propagation =  Photon_propagation(distribution[0],distribution[1], distribution[3], distribution[4], distribution[2], lens_radius,lens_height, lens_status, only_lens);
    //cout << "theta at the propagation " << propagation[2]*180/M_PI << endl;
    //cout << "phi at the propagation " << propagation[3]*180/M_PI << endl;
    //cout << "psix " << propagation[0] << endl;
    //cout << "psiy " << propagation[1] << endl;
    //cout << "position 1 x" << propagation[0] << endl;
    //cout << "position 1 y" << propagation[1] << endl;

    //Fill the TGraph2D of the intersection points
    g->SetPoint(i,intersection[0],intersection[1],intersection[2]);
    Projg->SetPoint(i,intersection[1],intersection[2]);

    //Fill the hit map
    Lens->Fill(propagation[0],propagation[1]);
    
    //Fill the angular distribution
    double angle = distribution[4]*180/M_PI;
    phi->Fill(angle);
    cphi+=1;

    //Definition of the` pixel geometry : 3 by 3 pixel
    
    //Pixel out of range 3x3 pixel (only needed if Hrect=0, otherwise it is cutting away photons at epoxy that could still reach the detector;
    if(Hrect==0 && (propagation[0]<0.0*pixel_sizex || propagation[0]>3.0*pixel_sizex || propagation[1] <0.0*pixel_sizey || propagation[1]>3*pixel_sizey)){
      list_of_out.push_back(i);
    }
    
    //Dead area in x
    else if(Hrect==0 && ((propagation[0]<dead_areax) || ((pixel_sizex-dead_areax)<propagation[0] && propagation[0]<(pixel_sizex+dead_areax)) || ((2.0*pixel_sizex-dead_areax)< propagation[0] && propagation[0] < (2.0*pixel_sizex+dead_areax)) || (propagation[0]>3*pixel_sizex-dead_areax))){
      bad->Fill(propagation[0],propagation[1]);
      deadcount+=1;
    }
    //Dead area in y
    else if(Hrect==0 && ((propagation[1]<dead_areay) || ((pixel_sizey-dead_areay)<propagation[1] && propagation[1]<(pixel_sizey+dead_areay)) || ((2.0*pixel_sizey-dead_areay)< propagation[1] && propagation[1] < (2.0*pixel_sizey+dead_areay)) || (propagation[1]>3*pixel_sizey-dead_areay))){
        bad->Fill(propagation[0],propagation[1]);
        deadcount+=1;
    }
    else if(propagation[0]==-1000 && propagation[1]==-1000){
        list_of_out.push_back(i); //CT
        //deadcount+=1; //ME
    }
    //Good hit in active area
    else {
        
        vector<double> pr2;
        pr2.clear();
        
        if(Hrect==0 || (propagation[0]==-1000 && propagation[1]==-1000))pr2=propagation;
        else{
            if(intersection[2]!=0 && lens_status==1)pr2 = rectunder(propagation[0], propagation[1], intersection[0], intersection[1],Hrect,intersection[2]);
            else pr2 = rect(propagation[0], propagation[1], distribution[0],distribution[1] ,Hrect, distance);
        }
        //Fill Photon distribution after interaction with epoxy
        Lens2->Fill(pr2[0],pr2[1]);    
        Lens2Proj=Lens2->ProjectionX();
        
        //Fill angular distribution after interaction with epoxy
        if(pr2[3] >= 0.00000001){
            //cout << "HEHEHEHEHE" << propagation[3] << endl;
            double anglep = pr2[3]*180/M_PI;
            phiprop->Fill(anglep);
            cphiprop+=1;
        }
        //cout << "theta after second propagation " << pr2[2]*180/M_PI << endl;
        //cout << "phi after second propagation " << pr2[3]*180/M_PI << endl;
        //cout << "psix " << pr2[0] << endl;
        //cout << "psiy " << pr2[1] << endl;

        //cout << "position 2 x" << pr2[0] << endl;
        //cout << "position 2 y" << pr2[1] << endl;
        
        if(pr2[0]<0.0*pixel_sizex || pr2[0]>3.0*pixel_sizex || pr2[1] <0.0*pixel_sizey || pr2[1]>3*pixel_sizey){
            list_of_out.push_back(i);
        }
        //Dead area in x
        else if ((pr2[0]<dead_areax) || ((pixel_sizex-dead_areax)<pr2[0] && pr2[0]<(pixel_sizex+dead_areax)) || ((2.0*pixel_sizex-dead_areax)< pr2[0] && pr2[0] < (2.0*pixel_sizex+dead_areax)) || (pr2[0]>3*pixel_sizex-dead_areax))
        {
          deadcount+=1;
          bad->Fill(pr2[0],pr2[1]);
          //bad2->Fill(pr2[0],pr2[1]);
        }
        //Dead area in <
        else if ((pr2[1]<dead_areay) || ((pixel_sizey-dead_areay)<pr2[1] && pr2[1]<(pixel_sizey+dead_areay)) || ((2.0*pixel_sizey-dead_areay)< pr2[1] && pr2[1] < (2.0*pixel_sizey+dead_areay)) || (pr2[1]>3*pixel_sizey-dead_areay))
        {
          deadcount+=1;
          bad->Fill(pr2[0],pr2[1]);
          //bad2->Fill(pr2[0],pr2[1]);
        }
        else if(pr2[0]==-1000 && pr2[1]==-1000){
            deadcount+=1;
        }
        else{
            goodcount+=1;
            good->Fill(pr2[0],pr2[1]);
            goodProj=good->ProjectionX();

        }
    }
  }

  //Fill projection of dead area
  if (Hrect==0) badProj=bad->ProjectionX();
  else badProj=bad2->ProjectionX();

  //Gives the number of photon hitting a dead area or an active area.
  //cout <<"Photon hitting dead area = " << deadcount << ", photon hitting active area = " << goodcount << "out of "<< goodcount+deadcount<< "events"<<endl;
  double tot_count = deadcount+ goodcount;

  cout << "Good Count " << goodcount << " Bad counts " << deadcount << endl;
  cout << "Achieved fill factor =" <<100* goodcount/tot_count << endl;
  cout << "LY increase =" << ((goodcount/tot_count)/GFF-1)*100 << endl;
  cout << "Of " << ::g_epoxy << " photons arriving at the epoxy " << ::g_epoxy_refl << " are reflected and " << ::g_epoxy_transm << " are transmitted." <<endl;

  //txt file
  char name[255];
  double lens_rad=lens_radius;
  //double sphere_rad=sphere_radius;
  sprintf(name,"/home/lphe/Lens/Lens_Carina/Results/output_Lens_radius%f_Lens_height%f_Hres%f.dat",lens_rad,lens_height,Hrect);
  ofstream results(name);
  results << "For a lens radius of " << lens_radius << "and a lens height of "<< lens_height << " we get: " << endl;
  results << "Photon hitting dead area = " << deadcount << ", photon hitting active area = " << goodcount << "out of "<< goodcount+deadcount<< "events"<<endl;
  results << "Of " << ::g_epoxy << " photons arriving at the epoxy " << ::g_epoxy_refl << " are reflected and " << ::g_epoxy_transm << " are transmitted." <<endl;
  results << " " << endl;
  results << "Achieved fill factor =" <<100* goodcount/tot_count << endl;
  results << "LY increase =" << ((goodcount/tot_count)/GFF-1)*100 << endl;

  //lines between pixels
  TLine *l = new TLine(pixel_sizex,0,pixel_sizex,3*pixel_sizey);
  TLine *l1 = new TLine(2*pixel_sizex,0,2*pixel_sizex,3*pixel_sizey);
  TLine *l2 = new TLine(0,pixel_sizey,3*pixel_sizex,pixel_sizey);
  TLine *l3 = new TLine(0,2*pixel_sizey,3*pixel_sizex,2*pixel_sizey);
  TLine *l4 = new TLine(pixel_sizex,0,pixel_sizex,3*pixel_sizey);
  
  //dashed line around dead area
  TLine *l_dead1 = new TLine(pixel_sizex-dead_areax,0,pixel_sizex-dead_areax,3*pixel_sizey);
  TLine *l_dead2 = new TLine(pixel_sizex+dead_areax,0,pixel_sizex+dead_areax,3*pixel_sizey);
  TLine *l_dead3 = new TLine(2*pixel_sizex-dead_areax,0,2*pixel_sizex-dead_areax,3*pixel_sizey);
  TLine *l_dead4 = new TLine(2*pixel_sizex+dead_areax,0,2*pixel_sizex+dead_areax,3*pixel_sizey);
  TLine *l_dead5 = new TLine(dead_areax,0,dead_areax,3*pixel_sizey);
  TLine *l_dead6 = new TLine(3*pixel_sizex-dead_areax,0,3*pixel_sizex-dead_areax,3*pixel_sizey);
  TLine *l_dead7 = new TLine(0,dead_areay,3*pixel_sizex,dead_areay);
  TLine *l_dead8 = new TLine(0,pixel_sizey-dead_areay,3*pixel_sizex, pixel_sizey-dead_areay);
  TLine *l_dead9 = new TLine(0,pixel_sizey+dead_areay,3*pixel_sizex, pixel_sizey+dead_areay);
  TLine *l_dead10 = new TLine(0,2*pixel_sizey-dead_areay,3*pixel_sizex,2*pixel_sizey-dead_areay);
  TLine *l_dead11 = new TLine(0,2*pixel_sizey+dead_areay,3*pixel_sizex,2*pixel_sizey+dead_areay);
  TLine *l_dead12 = new TLine(0,3*pixel_sizey-dead_areay,3*pixel_sizex,3*pixel_sizey-dead_areay);
    l_dead12->SetLineStyle(7);
    l_dead11->SetLineStyle(7);
    l_dead10->SetLineStyle(7);
    l_dead9->SetLineStyle(7);
    l_dead8->SetLineStyle(7);
    l_dead7->SetLineStyle(7);
    l_dead6->SetLineStyle(7);
    l_dead5->SetLineStyle(7);
    l_dead4->SetLineStyle(7);
    l_dead3->SetLineStyle(7);
    l_dead2->SetLineStyle(7);
    l_dead1->SetLineStyle(7);
    l_dead12->SetLineWidth(2);
    l_dead11->SetLineWidth(2);
    l_dead10->SetLineWidth(2);
    l_dead9->SetLineWidth(2);
    l_dead8->SetLineWidth(2);
    l_dead7->SetLineWidth(2);
    l_dead6->SetLineWidth(2);
    l_dead5->SetLineWidth(2);
    l_dead4->SetLineWidth(2);
    l_dead3->SetLineWidth(2);
    l_dead2->SetLineWidth(2);
    l_dead1->SetLineWidth(2);
  
  c1->cd();
  Photon_generation->Draw("colz");
  Photon_generation->GetXaxis()->SetTitle("x[#mum]");
  Photon_generation->GetYaxis()->SetTitle("y[#mum]");

  c2->cd();
  g->SetMarkerStyle(20);
  g->Draw("pcol");
  g->GetXaxis()->SetRangeUser(-50,210);
  g->GetYaxis()->SetRangeUser(-50,210); 

  c3->cd();
  Projg->SetMarkerSize(5);
  Projg->Draw("AP");
  Projg->GetXaxis()->SetRangeUser(0,120);
  char namec3[255];
  sprintf(namec3,"/home/lphe/Lens/Lens_Carina/Results/Interaction_Point_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c3->SaveAs(namec3);
  
  c11->cd();
  Lens->Draw("colz");
  Lens->GetXaxis()->SetTitle("x[#mum]");
  Lens->GetYaxis()->SetTitle("y[#mum]");
  l->Draw();
  l1->Draw();
  l2->Draw();
  l3->Draw();
  l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
  char namec11[255];
  sprintf(namec11,"/home/lphe/Lens/Lens_Carina/Results/Photon_Propagation_before_refraction_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c11->SaveAs(namec11);

    c9->cd();
    Lens2->Draw("colz");
    Lens2->GetXaxis()->SetTitle("x[#mum]");
    Lens2->GetYaxis()->SetTitle("y[#mum]");
    l->Draw();
    l1->Draw();
    l2->Draw();
    l3->Draw();
    l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
    char namec9[255];
    sprintf(namec9,"/home/lphe/Lens/Lens_Carina/Results/Photon_Propagation_after_refraction_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
    c9->SaveAs(namec9);
    
    c10->cd();
    Lens2Proj->Draw("bar");
    char namec10[255];
    sprintf(namec10,"/home/lphe/Lens/Lens_Carina/Results/Proj_Lens2_effect_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
    c10->SaveAs(namec10);

  c4->cd();
  bad->Draw("colz"); 
  bad->GetXaxis()->SetTitle("x[#mum]");
  bad->GetYaxis()->SetTitle("y[#mum]");
  l->Draw();
  l1->Draw();
  l2->Draw();
  l3->Draw();
  l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
  char namec4[255];
  sprintf(namec4,"/home/lphe/Lens/Lens_Carina/Results/Dead_area_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c4->SaveAs(namec4);

  c5->cd();
  good->Draw("colz");
  good->GetXaxis()->SetTitle("x[#mum]");
  good->GetYaxis()->SetTitle("y[#mum]");
  l->Draw();
  l1->Draw();
  l2->Draw();
  l3->Draw();
  l4->Draw();
    l_dead12->Draw();
    l_dead11->Draw();
    l_dead10->Draw();
    l_dead9->Draw();
    l_dead8->Draw();
    l_dead7->Draw();
    l_dead6->Draw();
    l_dead5->Draw();
    l_dead4->Draw();
    l_dead3->Draw();
    l_dead2->Draw();
    l_dead1->Draw();
  char namec5[255];
  sprintf(namec5,"/home/lphe/Lens/Lens_Carina/Results/Active_area_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c5->SaveAs(namec5);

  c6->cd();
  phi->Draw();
  good->GetXaxis()->SetTitle("x[#mum]");
  good->GetYaxis()->SetTitle("y[#mum]");

  c7->cd();
  Double_t scale_phiprop = phiprop->GetEntries();
  //cout << "hsuadiusajdiwa" << scale_phiprop<< endl;
  Double_t scale_phi = phi->GetEntries();
  phiprop->Draw();
  phiprop->Scale(1.0/scale_phiprop);
  phi->SetLineColor(kRed);
  phi->Draw("same");
  phi->Scale(1.0/scale_phi);
  char namec7[255];
  sprintf(namec7,"/home/lphe/Lens/Lens_Carina/Results/AngularDist_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c7->SaveAs(namec7);

  //c8->cd();
  //bad2->Draw("colz"); 
  //bad2->GetXaxis()->SetTitle("x[#mum]");
  //bad2->GetYaxis()->SetTitle("y[#mum]");
  //l->Draw();
  //l1->Draw();
  //l2->Draw();
  //l3->Draw();
  //l4->Draw();
  //l_dead12->SetLineStyle(2);
  //l_dead11->SetLineStyle(2);
  //l_dead10->SetLineStyle(2);
  //l_dead9->SetLineStyle(2);
  //l_dead8->SetLineStyle(2);
  //l_dead7->SetLineStyle(2);
  //l_dead6->SetLineStyle(2);
  //l_dead5->SetLineStyle(2);
  //l_dead4->SetLineStyle(2);
  //l_dead3->SetLineStyle(2);
  //l_dead2->SetLineStyle(2);
  //l_dead1->SetLineStyle(2);
  //l_dead12->Draw();
  //l_dead11->Draw();
  //l_dead10->Draw();
  //l_dead9->Draw();
  //l_dead8->Draw();
  //l_dead7->Draw();
  //l_dead6->Draw();
  //l_dead5->Draw();
  //l_dead4->Draw();
  //l_dead3->Draw();
  //l_dead2->Draw();
  //l_dead1->Draw();
//  char namec8[255];
//  sprintf(namec8,"/home/lphe/Lens/Lens_Carina/Results/Dead_Area2_Lens_radius%f_Lens_height%f.root",lens_rad,lens_height);
//  c8->SaveAs(namec8);
//
  c8->cd();
  badProj->Draw("bar");
  char namec8[255];
  sprintf(namec8,"/home/lphe/Lens/Lens_Carina/Results/Proj_dead_area_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c8->SaveAs(namec8);

  c12->cd();
  goodProj->Draw("bar");
  char namec12[255];
  sprintf(namec12,"/home/lphe/Lens/Lens_Carina/Results/Proj_active_area_Lens_radius%f_Lens_height%f_Hres%f.root",lens_rad,lens_height,Hrect);
  c12->SaveAs(namec12);

  return 0;
}



